﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M3UF4Proba_SanchezVictor
{
    public abstract class Pokemon : IPokemon 
    {
        public string _nombre;
        public string _tipoPokemon;
        public int _nivel;
        public int _vidaMaxima;
        public int _vidaActual; 

        public Pokemon (string nombre ,string tipoPok ,int nivel,int vidaMax,int vidaAct)
        {
            _nombre = nombre;
            _tipoPokemon = tipoPok;
            _nivel = nivel; 
            _vidaMaxima = vidaMax;
            _vidaActual = vidaAct;
        }

        public Pokemon()
        {
            _nombre = "";
            _tipoPokemon = "";
            _nivel = 0;
            _vidaMaxima = 0;
            _vidaActual = 0;
        }

        /// <summary>
        /// Metodo Abstracto donde segun el nivel y el Poder : 
        /// </summary>
        /// <returns></returns>
        public abstract int AtaquePoder(int nivel);

        /// <summary>
        /// Accion de restar vida al pokemon que atacamos : 
        /// </summary>
        /// <param name="obj"></param>
        public void Atacar(IPokemon obj)
        {
            Pokemon pokemon = (Pokemon)obj;

            //AtaquePoder(_nivel);
            pokemon._vidaActual -= _nivel;

            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine($"El Pokemon {_nombre} està atacando a {pokemon._nombre}");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"La vida actual del {pokemon._nombre} és {pokemon._vidaActual}");

        }

        public void Curar()
        {
            if (_vidaActual < _vidaMaxima && _vidaActual <= _vidaMaxima - 3)
            {
                _vidaActual += 3;
            }else if(_vidaActual < _vidaMaxima && _vidaActual >= _vidaMaxima-3)
            {
                _vidaActual = _vidaMaxima; 
            }
            Console.WriteLine($"El Pokemon se está curando {_vidaActual}");
        }
    }
}
