﻿using M3UF4Proba_SanchezVictor;
using System.Numerics;

public class PruebasEstadioPokemon
{
    private static void Main(string[] args)
    {

        //CREACION DE POKEMONS instancias a las clases segun el tipo de Pokemon :
        PokemonFuego player = new PokemonFuego("Charizard", "Fuego", 10, 100, 100, 70);         /*Charizard es Inmortal*/

        PokemonFuego pokeFuego = new PokemonFuego("Charmander", "Fuego", 15, 60, 60, 50);

        PokemonAgua pokeAgua = new PokemonAgua("Squirtle", "Agua", 9, 80, 80, 45);

        PokemonPlanta pokePlanta = new PokemonPlanta("Bulbasaur", "Planta", 2, 50, 50, 60);

        //string[] pokPick = new string[] { player._nombre, pokeFuego._nombre, pokeAgua._nombre, pokePlanta._nombre}; 

        //Bucle Partida Pokemon
        PartidaPokemon(player, pokePlanta);     //Modificar el nombre y el tipo de pokemon según la rivalida que queramos probar

        AtaqueElemental(pokeAgua, pokeFuego);

        AtaqueElemental(pokeFuego, pokePlanta);

        AtaqueElemental(pokePlanta, pokeAgua);

    }

    public static void PartidaPokemon(PokemonFuego player, PokemonPlanta pokePlanta)
    {
        bool pokemonDeath = false;

        Console.WriteLine("Bienvenido al ESTADIO POKEMON :");
        Console.WriteLine("");

        Console.WriteLine($"El pokemon con el que lucharas es: {player._nombre}");

        Console.WriteLine($"El pokemon contra el que enfrentas es: {pokePlanta._nombre}");

        do
        {
            JugadorInfo(player);


            string accion = Console.ReadLine();

            switch (accion.ToUpper())
            {
                case "A":
                    player.Atacar(pokePlanta); break;
                case "C":
                    player.Curar(); break;
            }

            IAinfo(pokePlanta); 

            if (pokePlanta._vidaActual > player._vidaMaxima/2)
            {
                pokePlanta.Atacar(player);
            }
            else if(pokePlanta._vidaActual < player._vidaMaxima / 2) pokePlanta.Curar();
                    

            if (player._vidaActual <= 0 || pokePlanta._vidaActual <= 0) pokemonDeath = true;

        } while (!pokemonDeath);
    }

    public static void JugadorInfo(PokemonFuego player)
    {
        Console.ForegroundColor = ConsoleColor.Blue;
        Console.WriteLine($"Turno de {player._nombre} - {player._vidaActual} /{player._vidaMaxima} HP : ");
        Console.ForegroundColor = ConsoleColor.White;

        Console.WriteLine($"Según la acción que desees realizar escribe una sola letra: ");
        Console.WriteLine($"A - Atacar ");
        Console.WriteLine($"C - Curarse ");
    }

    public static void IAinfo(PokemonPlanta pokePlanta)
    {
        Console.ForegroundColor = ConsoleColor.Blue;
        Console.WriteLine($"Turno de {pokePlanta._nombre} - {pokePlanta._vidaActual} /{pokePlanta._vidaMaxima} HP :");
        Console.ForegroundColor = ConsoleColor.White;
    }

    static void AtaqueElemental(IPokemon atacante, IPokemon defensor)
    {
        Pokemon pokemonAtaque = (Pokemon)atacante;
        Pokemon pokemonDefensa = (Pokemon)defensor;
        if (pokemonAtaque._tipoPokemon == "Agua" && pokemonDefensa._tipoPokemon == "Fuego")
        {
            pokemonDefensa._vidaActual -= pokemonAtaque.AtaquePoder(pokemonAtaque._nivel);
            Console.WriteLine($"Ataque Elemental de {pokemonAtaque._nombre} contra {pokemonDefensa._nombre} al que se le restan {pokemonAtaque.AtaquePoder(pokemonAtaque._nivel)} puntos");

        }
        else if (pokemonAtaque._tipoPokemon == "Fuego" && pokemonDefensa._tipoPokemon == "Planta")
        {
            pokemonDefensa._vidaActual -= pokemonAtaque.AtaquePoder(pokemonAtaque._nivel);
            Console.WriteLine($"Ataque Elemental de {pokemonAtaque._nombre} contra {pokemonDefensa._nombre} al que se le restan {pokemonAtaque.AtaquePoder(pokemonAtaque._nivel)} puntos");

        }
        else if (pokemonAtaque._tipoPokemon == "Planta" && pokemonDefensa._tipoPokemon == "Agua")
        {
            pokemonDefensa._vidaActual -= pokemonAtaque.AtaquePoder(pokemonAtaque._nivel);
            Console.WriteLine($"Ataque Elemental de {pokemonAtaque._nombre} contra {pokemonDefensa._nombre} al que se le restan {pokemonAtaque.AtaquePoder(pokemonAtaque._nivel)} puntos");

        }

    }
}

