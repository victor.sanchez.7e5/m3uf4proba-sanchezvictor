﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M3UF4Proba_SanchezVictor
{
    public interface IPokemon
    {
        void Atacar(IPokemon obj);
        void Curar();
    }
}
