﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M3UF4Proba_SanchezVictor
{
    public class PokemonFuego : Pokemon
    {
        int _poderFuego;

        public PokemonFuego(string nombre, string tipoPok, int nivel, int vidaMax, int vidaAct, int poderFuego ) : base (nombre, tipoPok, nivel, vidaMax, vidaAct)
        {
            _poderFuego = poderFuego;
        }

        public PokemonFuego()
        {

        }

        public override int AtaquePoder(int nivel)
        {
            int ataque = nivel * _poderFuego;

            return ataque;
        }
    }
}
