﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M3UF4Proba_SanchezVictor
{
    public class PokemonPlanta : Pokemon
    {
        int _poderPlanta;

        public PokemonPlanta(string nombre, string tipoPok, int nivel, int vidaMax, int vidaAct, int poderPlanta ) : base (nombre, tipoPok, nivel, vidaMax, vidaAct)
        {
            _poderPlanta = poderPlanta;
        }

        public override int AtaquePoder(int nivel)
        {
            int ataque = nivel * _poderPlanta;

            return ataque;
        }

    }
}
