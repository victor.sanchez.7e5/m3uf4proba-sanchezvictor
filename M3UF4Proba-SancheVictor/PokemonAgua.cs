﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M3UF4Proba_SanchezVictor
{
    public class PokemonAgua : Pokemon
    {
        int _poderAgua;

        public PokemonAgua(string nombre, string tipoPok, int nivel, int vidaMax, int vidaAct, int poderAgua ) : base (nombre, tipoPok, nivel, vidaMax, vidaAct)
        {
            _poderAgua = poderAgua;
        }

        public override int AtaquePoder(int nivel)
        {
            int ataque = nivel * _poderAgua;
            
            return ataque;
        }
    }
}
